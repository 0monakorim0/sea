import sea.AnimatedObject;
import sea.SeaObject;
import sea.World;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;


public class Sea {
    private static ArrayList<SeaObject> seaObjects = new ArrayList<SeaObject>();

    //, здесь лежат монеты (они есть и в seaObjects, но сюда дублируются чтобы проверять их
    /// пересечение
    /// с игроком)
    private static ArrayList<SeaObject> coins = new ArrayList<SeaObject>();

    public static BufferedImage resize(BufferedImage img, int newV, int newH) {
        Image tmp = img.getScaledInstance(newV, newH, Image.SCALE_SMOOTH);
        BufferedImage dimg = new BufferedImage(newV, newH, BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2d = dimg.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();

        return dimg;
    }

    public static void main(String[] args) throws Exception {
        JFrame window = new JFrame("My Sea");
    
        ///Добавляем фон
        File f = new File("image/more.png"); ///Создаём объект “файл” для работы с файлом изображения
        BufferedImage fon = ImageIO.read(f);
        BufferedImage background = fon.getSubimage(0, 0,
                255, 255);


        ///добавление звездочек
        f = new File("image/ras.png");
        BufferedImage stump = ImageIO.read(f);
        BufferedImage stumpImage = stump.getSubimage(42, 8,
                47, 51);
        int d = 0;
        while (d <= 35) {
            int x = (int) (Math.random() * 800);
            int y = (int) (Math.random() * 800);
            d += 1;
            seaObjects.add(new SeaObject(stumpImage, x, y));
        }

        World world = new World(background, seaObjects);
        ///добавляем деревья
        f = new File("image/ras.png");
        BufferedImage ras = ImageIO.read(f);
        BufferedImage rasImage = ras.getSubimage(62, 48,
                72, 84);
        BufferedImage rasImageScaled = resize(rasImage, 55, 65);
        int b = 0;
        while (b <= 3) {
            int x = (int) (Math.random() * 800);
            int y = (int) (Math.random() * 800);
            b += 1;
            SeaObject ras1 = new SeaObject(rasImageScaled, x, y);
            seaObjects.add(ras1);

        }
        ///5 штук деревьев
        int a = 0;
        while (a <= 5) {
            int x = (int) (Math.random() * 800);
            int y = (int) (Math.random() * 750);
            a += 1;
            seaObjects.add(new SeaObject(rasImage, x, y));
        }
        ///добавляем монеты
        f = new File("image/money.png");
        BufferedImage coin = ImageIO.read(f);
        BufferedImage moneyImage = coin.getSubimage(218, 23,
                                                      40, 44);
        BufferedImage moneyImageScaled = resize(moneyImage, 55, 65);
        a = 0;
        while (a <= 30) {
            int x = (int) (Math.random() * 800);
            int y = (int) (Math.random() * 800);
            a += 1;
            SeaObject mon = new SeaObject(moneyImage, x, y);
            seaObjects.add(mon);
             coins.add(mon);
        }
        ///добавляем игрока
        f = new File("image/pirategirl.png");
        BufferedImage playerImage = ImageIO.read(f); ///Считываем изображение из файла
        AnimatedObject player = new AnimatedObject(playerImage, 300, 100);

        seaObjects.add(player);

        window.addKeyListener(new KeyListener() {
       public void keyPressed(KeyEvent e) {
           
                    
             // здесь проверка пересечения монет с игроком
           for (int i = 0; i < coins.size(); i++) {
                    SeaObject coin = coins.get(i);
                    if ((player.getPosX() > coin.getPosX() - 5
                            && player.getPosX() < coin.getPosX() + 5)
                     && (player.getPosY() > coin.getPosY() - 5
                            && player.getPosY() < coin.getPosY() + 5)) {
                        World.coins++;
                        // убирает монету с карты
                        coins.remove(coin);
                        seaObjects.remove(coin); 
                    }
                }
            }

            public void keyReleased(KeyEvent e) {

            }

            public void keyTyped(KeyEvent e) {

            }
        });

      
        window.setContentPane(world);
        window.setSize(800, 800);
        window.setVisible(true);
      
        
                JButton L = new JButton("Left");
                    L.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                      player.move(-5, 0);
                     
                    } 
                    });
                JButton R = new JButton("Right");
                    R.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        player.move(5, 0);
                    }
                    });
                JButton U = new JButton("Up");
                    U.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        player.move(0, -5);
                     
                    }
                    }); 
                JButton D = new JButton("Down");
                    D.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        player.move(0, 5);
                        
                    }
                    });
        world.add(L, BorderLayout.NORTH);
        world.add(R, BorderLayout.CENTER);
        world.add(U, BorderLayout.SOUTH);
        world.add(D, BorderLayout.SOUTH);
       


    }
}